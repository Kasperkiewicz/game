import pygame

class Game():
    font = None
    state = 0 #0 init 1 - game 2 - game over 3 turning off
    zoomFactor = 4

    def __init__(self):
        self.state = 0
        self.font = pygame.font.SysFont(pygame.font.get_default_font(), int(round(16 * self.zoomFactor)))