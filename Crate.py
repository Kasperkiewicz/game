import pygame

class Crate():
    image = None
    inactiveImage = None
    active = True
    x = 0
    y = 0

    def __init__(self):
        self.image = pygame.image.load("resources/crate.png")
        self.inactiveImage = pygame.image.load("resources/crateinactive.png")

