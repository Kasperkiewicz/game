import csv
import os.path
import pygame
import Crate
import Hero


class Level():
    wallImage = None
    goalImage = None
    width = 0
    height = 0
    lvlNo = 0
    array = []
    hero = None
    crates = []

    def __init__(self):
        self.wallImage = pygame.image.load("resources/coble.png")
        self.goalImage = pygame.image.load("resources/goal.png")
        self.hero = Hero.Hero()
        self.lvlNo = 1;

    def findCrate(self, y, x):
        for crate in self.crates:
            if x == crate.x and y == crate.y:
                return crate

    def setLevel(self, no):
        filename = "levels/" + str(no) + ".csv"
        if os.path.exists(filename) == False:
            print("Level no " + str(no) + " does not exist")
            return False
        self.width = 0
        self.height = 0
        self.lvlNo = no
        self.crates = []
        self.crates.clear()
        self.array.clear()
        i = 0
        j = 0
        with open(filename) as levelFile:
            reader = csv.reader(levelFile, delimiter=',')
            for row in reader:
                self.width = len(row)
                self.height += 1
                self.array.append(row)
                for x in row:
                    if str(x) == "C":
                        tempCrate = Crate.Crate()
                        tempCrate.x = i
                        tempCrate.y = j
                        self.crates.append(tempCrate)
                    if str(x) == "H":
                        self.hero.x = i
                        self.hero.y = j
                    i += 1
                j += 1
                i = 0
            print("Height = " + str(self.height))
            print("Width = " + str(self.width))
            return True

    def draw(self, screen, zoomFactor):
        x = 0;
        y = 0;
        image = None
        for row in self.array:
            x = 0
            for column in row:
                if str(column) == "0":
                    x += 1
                    continue
                elif str(column) == "W":
                    image = self.wallImage
                elif str(column) == "G":
                    image = self.goalImage
                elif "H" in str(column):
                    image = self.hero.getImage()
                    self.hero.x = x
                    self.hero.y = y
                elif str(column) == "C" or str(column) == "GC":
                    image = self.crates[0].image
                image = pygame.transform.rotozoom(image, 0, zoomFactor)
                screen.blit(image, (x * image.get_width() + 1, y * image.get_height()))
                x += 1
            y += 1

    def moveHero(self, direction):
        oldx = self.hero.x
        oldy = self.hero.y
        newx = oldx
        newy = oldy
        nextx = 0
        nexty = 0
        if (direction == 'L'):
            newx -= 1
            nextx = newx - 1
            nexty = oldy
        elif (direction == 'R'):
            newx += 1
            nextx = newx + 1
            nexty = oldy
        elif (direction == 'U'):
            newy -= 1
            nexty = newy - 1
            nextx = oldx
        elif (direction == 'D'):
            newy += 1
            nexty = newy + 1
            nextx = oldx
        currentfield = self.array[oldy][oldx]
        newfield = self.array[newy][newx]
        nextfield = self.array[nexty][nextx]

        if newfield == 'W' or ((newfield == "C" or newfield == "GC") and (nextfield == "C" or nextfield == "GC")):
            return  # impossible to move
        elif newfield == '0' or newfield == 'G':  # just move
            self.hero.x = newx
            self.hero.y = newy
            if currentfield != "GH":
                self.array[oldy][oldx] = '0'
            else:
                self.array[oldy][oldx] = 'G'
            if newfield != 'G':
                self.array[newy][newx] = 'H'
            else:
                self.array[newy][newx] = 'GH'
            return
        elif (newfield == 'C' or newfield == 'GC') and nextfield != 'W' and nextfield != 'GC' and nextfield != 'C':
            self.hero.x = newx
            self.hero.y = newy
            if currentfield != "GH":
                self.array[oldy][oldx] = '0'
            else:
                self.array[oldy][oldx] = 'G'
            if newfield == 'C':
                self.array[newy][newx] = 'H'
                if nextfield != 'G':
                    self.array[nexty][nextx] = 'C'
                else:
                    self.array[nexty][nextx] = 'GC'
            elif newfield == 'GC':
                self.array[newy][newx] = 'GH'
                if nextfield != 'G':
                    self.array[nexty][nextx] = 'C'
                else:
                    self.array[nexty][nextx] = 'GC'
        return

    def checkGoal(self):
        print("check for level completion")
        for i in self.crates:
            if i.active == True:
                print("Not complete")
                return False
        print("Complete")
        return True
