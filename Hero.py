import pygame

class Hero():
    imageArray = []
    currentFrame = 2
    x = 0
    y = 0
    lives = 3

    def __init__(self):
        self.imageArray.append(pygame.image.load("resources/hero0.png").convert_alpha())
        self.imageArray.append(pygame.image.load("resources/hero1.png").convert_alpha())
        self.imageArray.append(pygame.image.load("resources/hero2.png").convert_alpha())
        self.imageArray.append(pygame.image.load("resources/hero3.png").convert_alpha())

    def getImage(self):
        return self.imageArray[self.currentFrame]

