import pygame as pygame,sys, os
from pygame.locals import *
from pygame import Color

import Hero
import Level
import Game
hero = None
zoomFactor = 4.8
font = None

def redraw(screen,level,game,hero):
    screen.fill(Color(0, 0, 0, 0))
    if game.state == 1:
        redrawgame(screen, level,hero)
        drawTexts(screen, hero, level, game)
    if game.state == 2:
        drawGameOver(screen,game)
    if game.state == 4:
        drawCongratulations(screen, game)
    pygame.display.update()
    pygame.display.flip()

def redrawgame(screen, level,hero):
    level.draw(screen, zoomFactor)

def drawGameOver(screen,game):
    text = game.font.render("Game Over", True, (255,255,255))
    screen.blit(text,((1024 - text.get_width())//2,(768 - text.get_height())//2))
    game.state = 3

def drawCongratulations(screen, game):
    text = game.font.render("Congratulations!", True, (255, 255, 255))
    screen.blit(text, ((1024 - text.get_width()) // 2, (768 - text.get_height()) // 2))
    text = game.font.render("You win!", True, (255, 255, 255))
    screen.blit(text, ((1024 - text.get_width()) // 2, (768 - (text.get_height()*2)) // 2))
    game.state = 3

def drawTexts(screen, hero, level, game):
    lvlText = "Level " + str(level.lvlNo)
    livesText = "Lives " + str(hero.lives)
    text = game.font.render(lvlText, True, (255,255,255))
    height = text.get_height()
    screen.blit(text,(1024 - text.get_width(), 768 - height))
    text = game.font.render(livesText, True, (255,255,255))
    screen.blit(text, ((1024 - text.get_width()), (768 - (height * 2))))


def main():
#Window creation
    pygame.init()
    window = pygame.display.set_mode((1024,768))
    pygame.display.set_caption('Sokoban')
    pygame.display.init()
    #initialize all variables
    hero = Hero.Hero()
    level = Level.Level()
    game = Game.Game()
    #Event definition
    level.setLevel(7)
    game.state = 1
    while True:
        for event in pygame.event.get():
            redraw(window,level,game,hero)
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT and game.state == 1:
                    level.moveHero('L')
                    if level.checkGoal():
                        level.lvlNo += 1
                        if  level.setLevel(level.lvlNo) == False:
                            game.state == 4
                elif event.key == pygame.K_RIGHT and game.state == 1:
                    level.moveHero('R')
                    if level.checkGoal():
                        level.lvlNo += 1
                        if  level.setLevel(level.lvlNo) == False:
                            game.state == 4
                elif event.key == pygame.K_UP and game.state == 1:
                    level.moveHero('U')
                    if level.checkGoal():
                        level.lvlNo += 1
                        if level.setLevel(level.lvlNo) == False:
                            game.state == 4
                elif event.key == pygame.K_DOWN and game.state == 1:
                    level.moveHero('D')
                    if level.checkGoal():
                        level.lvlNo += 1
                        if  level.setLevel(level.lvlNo) == False:
                            game.state == 4
                elif event.key == pygame.K_r and game.state == 1:
                    if (hero.lives - 1) < 0:
                        game.state = 2
                        #game over
                        print("You are dead end of game")
                    else:
                        game.state = 1
                        #reset level
                        hero.lives -= 1
                        level.setLevel(level.lvlNo)
            elif event.type == QUIT or game.state == 3 or game.state == 4:
                pygame.quit()
                sys.exit(0)

if __name__ == '__main__':
    main()





